
## Load Environments

```bash
loadenv()
{
         echo "Loading $1"
         for i in $(cat $1 | grep "^[^#;]"); do
            export $i
          done
}
```