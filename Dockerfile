FROM python:3.8-buster

ENV TUT_APP_DIR /app

RUN mkdir $TUT_APP_DIR

WORKDIR $TUT_APP_DIR

RUN apt-get update -y \  
    && apt-get update && apt-get install -y curl build-essential libssl-dev libffi-dev lib32ncurses5-dev git libsnappy-dev default-mysql-client postgresql-client\
    && apt-get install telnet net-tools 

ENV PYTHONPATH=$TUT_APP_DIR/

ENV POETRY_VIRTUALENVS_PATH=$TUT_APP_DIR/

RUN pip install poetry 

ADD pyproject.toml $TUT_APP_DIR/
ADD poetry.lock $TUT_APP_DIR/

RUN poetry install

ADD . $TUT_APP_DIR/

EXPOSE 3007

CMD ["/bin/bash", "-c", "./launch.sh"]