#!/usr/bin/env bash

PYTHONPATH=/app poetry run uvicorn tut.main:app --host 0.0.0.0 --port 3007 --workers 3